<?php

namespace app\controllers;

use app\models\Reward;
use app\models\User;
use app\models\UserRewardForm;
use Yii;
use yii\web\Controller;

class RewardController extends Controller
{
    /** @var User */
    private $user;

    public function beforeAction($action)
    {
        /** @var User $user */
        $this->user = Yii::$app->getUser()->identity;

        if (!$this->user) {
            $this->redirect('/');
        }

        return parent::beforeAction($action);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('reward_intro');
    }


    /**
     * Displays Random reward.
     *
     * @return string
     */
    public function actionShow()
    {
        $active_reward = Reward::getRandomFromActiveRewards();

        $user_reward_form = (new UserRewardForm())->setReward($active_reward);

        return $this->render('reward_form', [
            'user_reward_form' => $user_reward_form,
        ]);
    }

    public function actionGet()
    {
        $post = Yii::$app->request->post();

        if (!$post) {
            return $this->redirect('show');
        }

        $user_reward_form = new UserRewardForm();

        $transaction = Yii::$app->getDb()->beginTransaction();
        if ($user_reward_form->load($post) && $user_reward_form->submit()) {
            Yii::$app->session->setFlash('success', 'Got it!');
            $transaction->commit();
            return $this->redirect('/reward');
        }
        $transaction->rollBack();

        $message_type = 'error';
        Yii::$app->session->setFlash($message_type, "failed");

        return $this->redirect('/reward');
    }
}
