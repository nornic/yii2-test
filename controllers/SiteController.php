<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use app\models\LoginForm;

class SiteController extends Controller
{
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = 'main';

        if (!Yii::$app->user->isGuest) {
            return $this->redirect('/reward/show');
        }

        $login_form = new LoginForm();
        if ($login_form->load(Yii::$app->request->post()) && $login_form->login()) {

            return $this->redirect('/reward/show');
        }

        return $this->render('login', [
            'login_form' => $login_form,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect('login');
    }
}
