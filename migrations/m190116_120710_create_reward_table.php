<?php

use yii\db\Migration;

/**
 * Handles the creation of table `reward`.
 */
class m190116_120710_create_reward_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('reward', [
            'id' => $this->primaryKey(),
            'type' => $this->string()->notNull(),
            'title' => $this->string()->notNull(),
            'exchange_reward_id' => $this->integer(),
            'exchange_ratio' => $this->integer(),
            'stock' => $this->integer(),
            'transaction_limit' => $this->integer()->notNull(),
            'date_created' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('reward');
    }
}
