<?php

use yii\db\Migration;
use app\models\User;

/**
 * Class m190116_120832_fill_user_table
 */
class m190116_120832_fill_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $user_count = rand(3,10);
        for ($i = 1; $i <= $user_count; ++$i) {
            $user = new User();
            $user->email = 'user_'.$i."@mail.com";
            $user->setPassword('111111');
            $user->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->truncateTable('user');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190116_120832_fill_user_table cannot be reverted.\n";

        return false;
    }
    */
}
