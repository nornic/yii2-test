<?php

use yii\db\Migration;
use app\models\Reward;

/**
 * Class m190116_120908_fill_reward_table
 */
class m190116_120908_fill_reward_table extends Migration
{
    private $fixtures = [
        [
            'id' => '1',
            'type' => Reward::TYPE_SCORE,
            'title' => 'Score',
            'transaction_limit' => 1000
        ],
        [
            'id' => '2',
            'type' => Reward::TYPE_MONEY,
            'title' => 'Money',
            'exchange_reward_id' => 1,
            'exchange_ratio' => 1000,
            'stock' => '1000000000',
            'transaction_limit' => 100
        ],
        [
            'type' => Reward::TYPE_GIFT,
            'title' => 'Gift 7',
            'stock' => '10',
            'transaction_limit' => 1
        ],
        [
            'type' => Reward::TYPE_GIFT,
            'title' => 'Gift 6',
            'stock' => '10',
            'transaction_limit' => 1
        ],
        [
            'type' => Reward::TYPE_GIFT,
            'title' => 'Gift 5',
            'stock' => '10',
            'transaction_limit' => 1
        ],
        [
            'type' => Reward::TYPE_GIFT,
            'title' => 'Gift 4',
            'stock' => '10',
            'transaction_limit' => 1
        ],
        [
            'type' => Reward::TYPE_GIFT,
            'title' => 'Gift 3',
            'stock' => '10',
            'transaction_limit' => 1
        ],
        [
            'type' => Reward::TYPE_GIFT,
            'title' => 'Gift 2',
            'stock' => '10',
            'transaction_limit' => 1
        ],
        [
            'type' => Reward::TYPE_GIFT,
            'title' => 'Gift 1',
            'stock' => '10',
            'transaction_limit' => 1
        ],
    ];
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        foreach ($this->fixtures as $fixture) {
            $reward = new Reward();
            $reward->load($fixture, '');
            $reward->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->truncateTable('reward');

    }

}
