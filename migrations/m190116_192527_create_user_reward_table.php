<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_reward`.
 */
class m190116_192527_create_user_reward_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user_reward', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'reward_id' => $this->integer()->notNull(),
            'amount' => $this->integer()->notNull(),
            'date_created' => $this->integer()->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user_reward');
    }
}
