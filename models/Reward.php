<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Class Reward
 * @package app\models
 *
 * @property int $id
 * @property int $type
 * @property string $title
 * @property int $exchange_reward_id
 * @property int $exchange_ratio
 * @property int $stock
 * @property int $transaction_limit
 * @property int $date_created
 *
 */
class Reward extends ActiveRecord
{
    /** @var int */
    private $withdrawal= null;

    const TYPE_MONEY = 10;
    const TYPE_SCORE = 20;
    const TYPE_GIFT = 30;

    const TYPE_MONEY_TEXT = 'Money';
    const TYPE_SCORE_TEXT = 'Score';
    const TYPE_GIFT_TEXT = 'Gift';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reward';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['type'], 'required'],
            [['id', 'type', 'exchange_reward_id', 'exchange_ratio', 'stock', 'date_created', 'transaction_limit'], 'integer', 'min' => 0],
            [['title'], 'string'],
            [['exchange_reward_id'], 'validateExchange']
        ];

    }

    public function validateExchange($attribute): bool
    {
        $exchange_reward = Reward::findOne($this->exchange_reward_id);
        if (!$exchange_reward) {
            $this->addError($attribute, 'Does not exist');

            return false;
        }

        if ($exchange_reward->type === $this->type) {
            $this->addError($attribute, 'Invalid exchange reward type');

            return false;
        }

        return true;
    }

    public function beforeSave($insert)
    {
        $this->date_created = $this->date_created ?? time();

        return parent::beforeSave($insert);
    }

    public function getRandomAmount():int
    {
        return rand(1, $this->getAllowedAmount());
    }

    /**
     * @param int|null $amount_requested
     * @return int
     */
    public function getAllowedAmount(int $amount_requested = null):int
    {
        if ($amount_requested && $amount_requested < $this->stock && $amount_requested < $this->transaction_limit) {

            return $amount_requested;
        }

        if ($this->stock !== null && $this->stock < $this->transaction_limit) {

            return $this->stock;
        }

        return $this->transaction_limit ;
    }

    public function isConvertible():bool
    {
        return !empty($this->exchange_ratio) && !empty($this->exchange_reward_id);
    }

    /**
     * @param int $amount
     * @return bool
     */
    public function decreaseStock(int $amount):bool
    {
        if ($this->stock === null) {
            return true;
        }

        $this->stock = $this->stock - $amount;

        return $this->save();

    }


    /**
     * @return Reward
     */
    public static function getRandomFromActiveRewards():? Reward
    {
        /** @var Reward[] $active_rewards */
        $active_rewards = Reward::find()
            ->where(['>', 'stock', 0])
            ->all();
        $rewards_count = count($active_rewards);

        if ($rewards_count <= 0) {

            return null;
        }

        return $active_rewards[rand(0, $rewards_count-1)];
    }

    public function setWithdrawal(int $amount):self
    {
        $this->withdrawal = $amount;
        return $this;
    }

    public function getWithdrawal():?int
    {
        return $this->withdrawal;
    }





}
