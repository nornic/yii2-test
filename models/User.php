<?php

namespace app\models;

use yii\web\IdentityInterface;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property int $date_created
 */
class User extends ActiveRecord implements IdentityInterface
{
    /** @var  User */
    public static $user;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    public static function getPasswordSalt():string
    {
        return 'test';
    }

    /**
     * @param string $password
     * @return string
     */
    public function setPassword($password):User
    {
        $this->password = md5($password.User::getPasswordSalt());
        return $this;
    }


    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['password', 'email'], 'required'],
            [['date_created'], 'integer'],
            [['password', 'email'], 'string'],
        ];

    }

    public function beforeSave($insert)
    {
        $this->date_created = $this->date_created ?? time();
        return parent::beforeSave($insert);
    }

    public function validatePassword($password): bool
    {
        return $this->password === md5($password . self::getPasswordSalt());
    }

    /**
     * @param string $email
     * @return static|null
     */
    public static function findByEmail(string $email):? User
    {
        return static::findOne(['email' => $email]);
    }



    /**
     * Finds an identity by the given ID.
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id) {
        if (empty(static::$user)) {
            static::$user = static::findOne($id);
        }

        return static::$user;
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        // TODO: Implement findIdentityByAccessToken() method.
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|integer an ID that uniquely identifies a user identity.
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey() {
        return $this->authKey;
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return boolean whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey) {
        return $this->getAuthKey() == $authKey;
    }

}
