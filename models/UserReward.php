<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Class Reward
 * @package app\models
 * @property int $user_id
 * @property int $reward_id
 * @property int $amount
 * @property int $date_created
 *
 */
class UserReward extends ActiveRecord
{

    const STATUS_PENDING = 10;
    const STATUS_COMMITTED = 20;
    const STATUS_DECLINED = 30;
    const STATUS_CANCELED = 40;


    /** @var Reward */
    private $reward = null;
    /** @var User */
    private $user = null;

    /** @var integer */
    private $requested_reward = null;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amount', 'reward_id', 'user_id', 'date_created'], 'integer'],
        ];
    }


    public function beforeSave($insert)
    {
        $this->date_created = $this->date_created ?? time();

        return parent::beforeSave($insert);
    }

    public function setReward(Reward $reward):self
    {
        $this->reward_id = $reward->id;
        $this->reward = $reward;

        return $this;
    }

    public function setUser(User $user):self
    {
        $this->user_id = $user->id;
        $this->user = $user;

        return $this;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function setRequestedReward($amount):self
    {
        $this->requested_reward = $amount;
    }

    public function getRequestedReward(): int
    {
        return $this->requested_reward;
    }




}
