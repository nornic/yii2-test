<?php

namespace app\models;

use app\models\processors\RewardFactory;
use yii\base\Model;

/**
 * Class RewardForm
 * @package app\models
 *
 */
class UserRewardForm extends Model
{
    /** @var bool */
    public $is_convert_to = false;
    public $amount;
    public $reward_id;


    /** @var Reward */
    private $reward;
    /** @var User */
    private $user;

    /** @var Reward array */
    private $exchange_reward_list = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'reward_id', 'amount'], 'required'],
            [['user_id', 'reward_id', 'amount'], 'integer'],
            [['is_convert_to'], 'boolean'],
            [['amount'], 'validateAmount']
        ];
    }

    public function validateAmount($attribute)
    {
        if (!$this->reward) {
            $this->addError($attribute,'Error');
            return;
        }

        if ($this->amount > $this->reward->getAllowedAmount()) {
            $this->addError($attribute,'Error');
            return;
        }
    }

    public function load($data, $formName = null)
    {
        if (!parent::load($data, $formName)) {
            return false;
        }

        $this->setReward(Reward::findOne($this->reward_id));

        return (bool)$this->reward;
    }



    public function __construct()
    {
        $this->user = \Yii::$app->getUser()->identity;

        parent::__construct();
    }

    public function setReward(?Reward $reward):self
    {
        if (empty($reward)) {
            return $this;
        }

        $this->reward = $reward;
        $this->reward_id = $this->reward_id ?? $reward->id;

        $this->amount = $this->amount ?? $this->reward->getRandomAmount();

        if ($this->reward->isConvertible()) {
            $this->exchange_reward_list = $this->getExchangeRewardList();
        }

        return $this;
    }


    /**
     * @return array
     */
    private function getExchangeRewardList():array
    {
        $exchange_reward_list = [];

        $exchange_reward = Reward::findOne($this->reward->exchange_reward_id) ?? null;

        $exchange_reward_amount = $this->amount * $this->reward->exchange_ratio;
        $exchange_reward_available = $exchange_reward->getAllowedAmount($exchange_reward_amount);

        $exchange_reward_list[] = $exchange_reward->setWithdrawal($exchange_reward_available);
        $reward_offset = floor($this->amount - $exchange_reward_available / $this->reward->exchange_ratio);

        if ($exchange_reward_amount > $exchange_reward_available && $reward_offset > 0) {
            $exchange_reward_list[] = $this->reward->setWithdrawal($reward_offset);
        }

        return $exchange_reward_list;
    }


    public function getRewardText():string
    {
        if (!$this->isInStock()) {
            return 'Sorry rewards are out of stock';
        }

        return "You win  $this->amount pcs of '". strtolower($this->reward->title)."'";
    }

    public function isInStock():bool
    {
        return $this->amount > 0;
    }

    public function getConversionText():?string
    {
        if (!$this->exchange_reward_list) {
            return null;
        }
        $message = "Convert your reward to ";
        $add_message = [];

        /** @var Reward $reward */
        foreach ($this->exchange_reward_list as $reward) {
            $add_message[] = $reward->getWithdrawal() . "pcs. of '$reward->title'";
        }
        
        return $add_message ? $message. implode(' and ', $add_message) : null;
    }


    /**
     * @return bool
     */
    public function submit():bool
    {
        if (!$this->user || !$this->reward) {
            return false;
        }

        if (!$this->is_convert_to) {
            return RewardFactory::getRewardProcessor($this->reward)->process($this->amount);
        }

        /** @var Reward $reward */
        foreach ($this->exchange_reward_list as $reward) {
            if (!RewardFactory::getRewardProcessor($reward)->process($reward->getWithdrawal())) {
                return false;
            }
        }

        return true;
    }

}
