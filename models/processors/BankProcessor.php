<?php

namespace app\models\processors;

use app\components\Banking;

class BankProcessor extends RewardFactory
{
    protected function send():bool
    {
        return (new Banking())->sendPayment($this->user, $this->reward_amount);
    }

}
