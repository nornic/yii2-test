<?php

namespace app\models\processors;

use app\models\Reward;
use app\models\User;
use app\models\UserReward;
use Yii;

abstract class RewardFactory
{
    /** @var Reward  */
    protected $reward;
    /** @var User */
    protected $user;

    protected $reward_amount;

    private static $process_router = [
        Reward::TYPE_MONEY => BankProcessor::class,
        Reward::TYPE_GIFT => GiftProcessor::class,
        Reward::TYPE_SCORE => ScoreProcessor::class,
    ];


    abstract protected function send();


    public function __construct(Reward $reward, User $user = null)
    {
        $this->user = $user ?? Yii::$app->getUser()->identity;
        $this->reward = $reward;
    }

    public function process(int $amount):bool
    {
        if ($amount <= 0 ) {
            return false;
        }

        $this->reward_amount = $amount;
        $user_reward = (new UserReward())
            ->setReward($this->reward)
            ->setUser($this->user)
            ->setAmount($amount);

        return $user_reward->save() && $this->reward->decreaseStock($amount) && $this->send();
    }

    public static function getRewardProcessor(Reward $reward, User $user = null):?self
    {
        $class_name = self::$process_router[$reward->type] ?? null;

        return new $class_name($reward, $user);
    }

}
