<?php
use yii\bootstrap\ActiveForm;
use app\models\UserRewardForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $form yii\bootstrap\ActiveForm */
/* @var $this yii\web\View */
/* @var $user_reward_form UserRewardForm*/



$this->title = 'TAKE YR REWARD';
$this->params['breadcrumbs'][] = $this->title;
?>
    <h1 class="text-center"><?= Html::encode($this->title) ?></h1>

<br><br><br>

<?php $form = ActiveForm::begin([
        'action' => Url::to(['/reward/get'])
]); ?>

<div class="form-group text-center">
    <h3><?=$user_reward_form->getRewardText()?></h3>

    <?php if ($user_reward_form->isInStock()): ?>
        <?php if ($user_reward_form->getConversionText()): ?>

            <?= $form->field($user_reward_form, 'amount')->hiddenInput()->label(false);?>
            <?= $form->field($user_reward_form, 'reward_id')->hiddenInput()->label(false);?>
            <?= $form->field($user_reward_form, 'is_convert_to')->checkbox([
                'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
            ])->label($user_reward_form->getConversionText()) ?>
        <?php endif;?>

        <?= Html::submitButton('Get reward', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        <a href="<?= Url::to('/reward/')?>">
            <?= Html::button('Dismiss', ['class' => 'btn btn-warning', 'name' => 'login-button']) ?>
        </a>
    <?php endif;?>

</div>


<?php ActiveForm::end(); ?>



